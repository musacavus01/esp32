#define DEBUG true

void printData(String description) {
  Serial.println(description);
}

void printData(String description, int value) {
  if (!DEBUG) return;
  Serial.println(description + ": " + value);
}

void printData(String description, float value) {
  if (!DEBUG) return;
  Serial.println(description + ": " + value);
}

void printData(String description, String value) {
  if (!DEBUG) return;
  Serial.println(description + ": " + value);
}

void printData(String description, bool value) {
  if (!DEBUG) return;
  Serial.println(description + ": " + value);
}

void printData(String description, char* value) {
  if (!DEBUG) return;
  Serial.println(description + ": " + value);
}

void printData(String description, long unsigned int value) {
  if (!DEBUG) return;
  Serial.println(description + ": " + value);
}
