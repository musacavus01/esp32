#include <WebSocketsClient.h>

#include "BLEDevice.h"

static BLEUUID serviceUUID("0000180a-0000-1000-8000-00805f9b34fb");
static BLEUUID charUUID("00000002-0000-1000-8000-00805f9b34fb");

static BLERemoteCharacteristic* pRemoteCharacteristic;
static BLERemoteService* pRemoteService;
static BLEAdvertisedDevice* myDevice;
BLEClient*  pClient;

static boolean isBleClient = false;
static String cmd = "1";

static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;

WebSocketsClient webSocket;
#define SERVER_ADDR "167.71.40.136"
#define SERVER_PORT 4001
#define HEARTBEAT_INTERVAL 300000

uint64_t heartbeatTimestamp = 0;


class MyClientCallback : public BLEClientCallbacks {
    void onConnect(BLEClient* pclient) {
      Serial.println("client connect");
    }

    void onDisconnect(BLEClient* pclient) {
      // connected = false;
      Serial.println("client disconnect");
    }
};

bool connectToServer() {
  Serial.print("Forming a connection to ");
  Serial.println(myDevice->getAddress().toString().c_str());

  pClient = BLEDevice::createClient();
  Serial.println(" - Created client");

  pClient->setClientCallbacks(new MyClientCallback());

  // Connect to the remove BLE Server.
  pClient->connect(myDevice); // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)

  Serial.println(pClient->isConnected());
  if (!pClient->isConnected()) {
    pClient->disconnect();
    return false;
  }
  Serial.println(" - Connected to server");

  // Obtain a reference to the service we are after in the remote BLE server.
  pRemoteService = pClient->getService(BLEUUID("0000fef9-1212-efde-1523-785fef13d123"));
  Serial.println("servise gelidm");
  if (pRemoteService == nullptr) {
    Serial.print("Failed to find our service UUID: ");
    Serial.println(serviceUUID.toString().c_str());
    pClient->disconnect();
    return false;
  }
  Serial.println(" - Found our service");


  pRemoteCharacteristic = pRemoteService->getCharacteristic(BLEUUID("00000002-0000-1000-8000-00805f9b34fb"));
  Serial.println("car gelidm");

  if (pRemoteCharacteristic == nullptr) {
    Serial.print("Failed to find our characteristic UUID: ");
    Serial.println(charUUID.toString().c_str());
    pClient->disconnect();
    return false;
  }
  Serial.println(" - Found our characteristic");

  if (pRemoteCharacteristic->canRead()) {
    std::string value = pRemoteCharacteristic->readValue();
    Serial.print("The characteristic value was: ");
    Serial.println(value.c_str());
  }

  // if (pRemoteCharacteristic->canNotify())
  // pRemoteCharacteristic->registerForNotify(notifyCallback);

  connected = true;
  return true;
}

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      Serial.print("BLE Advertised Device found: ");
      Serial.println(advertisedDevice.toString().c_str());

      if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {
        BLEDevice::getScan()->stop();
        myDevice = new BLEAdvertisedDevice(advertisedDevice);
        doConnect = true;
        doScan = true;
        isBleClient = true;
      }
    }
};

void setupClientBle() {
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);
  pBLEScan->start(10, false);
}

void controlTheLock(String e, String mac) {
  preferences.begin("Settings", false);
  String mac_address = preferences.getString("bleMacAddress", "0");
  preferences.end();

  if (mac == mac_address) { // Check if command belongs to this lock
    if (e == "openLock") {
      // doorOpen();
      cmd = "1";
      setupClientBle();
    }

    if (e == "closeLock") {
      // doorClose();
      cmd = "0";
      setupClientBle();
    }
  }


}

void webSocketEvent(WStype_t type, uint8_t* payload, size_t length) {
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.printf("[WSc] Webservice disconnected from the NoQey server!\n");
      break;
    case WStype_CONNECTED: {
        Serial.printf("[WSc] Service connected to the NoQey server at url: %s\n", payload);
        Serial.printf("Waiting for commands from the NoQey server ...\n");
      }
      break;
    case WStype_TEXT: {
        Serial.printf("[WSc] get text: %s\n", payload);

        String text = ((const char *)&payload[0]);
        String json;

        int pos = text.indexOf('[');

        if (pos == 2) {
          json = text.substring(pos);
        }

        const size_t capacity = JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(5) + 150;
        DynamicJsonDocument doc(capacity);

        DeserializationError error = deserializeJson(doc, json);

        if (error) {
          Serial.print(F("deserializeJson() failed: "));
          Serial.println(error.c_str());

          return;
        }

        const String event = doc[0];

        const String room = doc[1]["room"];

        JsonObject lock = doc[1]["door"];
        const String lock_name = lock["name"];
        const String mac_address = lock["macAddress"];

        controlTheLock(event, mac_address);
      }
      break;
    case WStype_BIN:
      Serial.printf("[WSc] get binary length: %u\n", length);
      break;
  }
}

void setupWebSocket() {
  // server address, port and URL
  webSocket.begin(SERVER_ADDR, SERVER_PORT, "/socket.io/?transport=websocket");
  // event handler
  webSocket.onEvent(webSocketEvent);

  // try again every 5000ms if connection has failed
  // webSocket.setReconnectInterval(1000);
  webSocket.enableHeartbeat(1000, 9000, 99);
}

void loopWebSocket() {
  webSocket.loop();

  // uint64_t now = millis();

  // Send heartbeat in order to avoid disconnections during ISP resetting IPs over night. Thanks @MacSass
  // if ((now - heartbeatTimestamp) > HEARTBEAT_INTERVAL) {
  //  heartbeatTimestamp = now;
  //  webSocket.sendTXT("connected");
  // }
}


void clientBleLoop() {
  if (doConnect == true) {
    if (connectToServer()) {
      Serial.println("We are now connected to the BLE Server.");
      String newValue = cmd;
      pRemoteCharacteristic->writeValue(newValue.c_str(), newValue.length());
      delay(1800);
      pClient->disconnect();
      doConnect = false;
      doScan = false;
      myDevice = NULL;
      pRemoteService = NULL;
      pRemoteCharacteristic = NULL;
      pClient = NULL;
    } else {
      Serial.println("We have failed to connect to the server; there is nothin more we will do.");
    }
    doConnect = false;
  }
}
