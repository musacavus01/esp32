// motor bacaklari
#define PWMA 15
#define AIN2 2
#define AIN1 4
#define STBY 16

boolean isMotorRunning = false;
float MotorKalkisVoltaji = 0;

float MotorDurmaYuzdesi(float pilVoltaji) {
  // return ((pilVoltaji - PIL_MAX - (((YUZDE_MAX - YUZDE_MIN) / (YUZDE_MIN)) * (PIL_MAX - PIL_MIN))) * ((YUZDE_MIN - YUZDE_MAX) / (PIL_MAX - PIL_MIN))) / 100;
  return (YUZDE_MAX - ((YUZDE_MAX - YUZDE_MIN) / (PIL_MAX - PIL_MIN)) * (pilVoltaji - PIL_MIN)) / 100;
}

void motorStop() {
  printData("Motor Stopped!..");
  digitalWrite(PWMA, LOW);
  digitalWrite(AIN2, LOW);
  digitalWrite(AIN1, LOW);
  digitalWrite(STBY, LOW);
  isMotorRunning = false;
}

void doorOpen() {
  motorStop();
  printData("---------------------Kilit açıldı!..");
  delay(100);
  digitalWrite(PWMA, HIGH);
  digitalWrite(AIN2, HIGH);
  digitalWrite(AIN1, LOW);
  digitalWrite(STBY, HIGH);
  isMotorRunning = true;
  delay(100);
  MotorKalkisVoltaji = MotorVoltaj();
  delay(1800);
  motorStop();
  printData("Motor Voltaji", MotorKalkisVoltaji);
}

void doorClose() {
  motorStop();
  printData("---------------------Kilitlendi!..");
  delay(100);
  digitalWrite(PWMA, HIGH);
  digitalWrite(AIN2, LOW);
  digitalWrite(AIN1, HIGH);
  digitalWrite(STBY, HIGH);
  isMotorRunning = true;
  MotorKalkisVoltaji = MotorVoltaj();
  delay(1800);
  motorStop();
  printData("Motor Voltaji", MotorKalkisVoltaji);
}
