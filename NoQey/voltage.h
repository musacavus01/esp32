const float PIL_MAX = 9; // Volt
const int YUZDE_MIN = 10; // Yüzde
const float PIL_MIN = 7.0; // Volt
const int YUZDE_MAX = 20; // Yüzde
/*
   Bu fonksiyon dolu ve boş pile göre motorun durma voltajını ayaralamk için yüzde hesabı yapar
   Eğer dolu pilde motor durmuyorsa YUZDE_MIN değeri küçültülür. Eğer dolu pilde motor düşük dirençte
   hemen duruyorsa YUZDE_MIN değeri büyültülür

   Eğer boş pilde motor durmuyorsa YUZDE_MAX değeri küçültülür. Eğer boş pilde motor düşük dirençte
   hemen duruyorsa YUZDE_MAX değeri büyültülür
*/

//-------VOLTAJ PARAMETRELERİ--------------------------------------------
#define pil_voltaj_pini 36

//------- AKIM PARAMETRELERİ--------------------------------------------
#define motor_voltaj_pini 39

float pil() {
  float vout = 0.0;
  float vin = 0.0;
  float R1 = 2190;   // 2K ohm direnc
  float R2 = 1010;   // 1K ohm direnc

  long int sum_voltaj = 0;
  
  sum_voltaj = analogRead(pil_voltaj_pini);
  vout = (sum_voltaj * 3.3) / 4095.0;
  // printData("Pil Volt", vout);
  vin = vout / (R2 / (R1 + R2));

  return vin;
}

float MotorVoltaj()
{
  float MR2 = 2190;    // 2K ohm direnc
  float MR1 = 1010;    // 1K ohm direnc

  uint16_t mAdc = analogRead(motor_voltaj_pini);
  // printData("mAdc", mAdc);
  float motorReadVolt = (mAdc * 3.3) / 4095.0;
  // printData("motorReadVolt", motorReadVolt);
  float MotorActualVolt = motorReadVolt / (MR1 / (MR1 + MR2));
  // printData("MotorActualVolt", MotorActualVolt);

  return MotorActualVolt;
}
