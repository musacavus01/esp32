boolean isDoorOpen = false;
boolean triggerNotification = false;

long durationToSendAgain = 10000;
long openTimer = 0;

void sendNotificationOverFCM() {
  HTTPClient http;
  
  preferences.begin("Settings", false);
  //preferences.putString("bleMacAddress", "30:AE:A4:97:64:76");
  String bleMacAddress = preferences.getString("bleMacAddress");
  preferences.end();

  if (bleMacAddress == "") {
    bleMacAddress = "30:AE:A4:97:64:76";
  }

  printData("MAC ADDRESS", bleMacAddress);
  
  http.begin("185.226.160.93", 3001, "/api/v1/user/notification");
  http.addHeader("Content-Type", "application/json; charset=utf-8");

  const size_t capacity = JSON_OBJECT_SIZE(4) + 100;
  String json;
  
  DynamicJsonDocument doc(capacity);
  
  doc["type"] = "esp";
  doc["title"] = "NoQey - Kapı Durum Güncellemesi";
  doc["content"] = "Evinize anahtar kullanılarak giriş yapılmıştır.";
  doc["macAddress"] = bleMacAddress;
  
  serializeJson(doc, json);

  int httpCode = http.POST(json);

}

void sendDoorNotification(String status) {
  if (status == "closed") {
    isDoorOpen = false;
  } else {
    if (isDoorOpen == false) {
      isDoorOpen = true;
  
      if (triggerNotification == false) {
        openTimer = millis();
        triggerNotification = true;
        sendNotificationOverFCM();
      }
    } else {
      if ((millis() - openTimer > durationToSendAgain) && (triggerNotification == true)) {
        triggerNotification = false;
        sendNotificationOverFCM();
      }
    }
  } 
}
