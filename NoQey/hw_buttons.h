int bootBtn = 0;

long buttonTimer = 0;
long longPressTime = 6000;

boolean buttonActive = false;
boolean longPressActive = false;

void setupHardwareButtons() {
  pinMode(bootBtn, INPUT);
}

void checkHardwareButtons() {
  if (digitalRead(bootBtn) == LOW) {

    if (buttonActive == false) {
      buttonActive = true;
      buttonTimer = millis();
    }

    if ((millis() - buttonTimer > longPressTime) && (longPressActive == false)) {
      longPressActive = true;
      printData("-------------------------------------------LONG PRESSED!..", millis() - buttonTimer);
      preferences.begin("Settings", false);
      preferences.clear();
      preferences.end();
    }

  } else {
    if (buttonActive == true) {
      if (longPressActive == true) {
        longPressActive = false;
      } else {
        printData("-------------------------------------------SHORT PRESSED!..");
      }

      buttonActive = false;
    }
  }
}
