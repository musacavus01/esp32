#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer* pServer = NULL;
BLEService *pService;

BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;

String bleValue;

static BLEUUID SERVICE_UUID("d24ed415-1a95-4a82-974e-d811e913abb7");
static BLEUUID CHARACTERISTIC_UUID("1e4807aa-49a8-45d9-9777-caa52fe4738c");

// Encryption
const String alphabet = "abcdefghijklmnopqrstuvwxyz";
const String alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const String fullAlphabet = alphabet + alphabet + alphabet;
const int cipherOffset = 61 % 26;

void evaluateCommands(char key, String cmd) {
  switch (key) {
    case '0':
      doorOpen();
      break;
    case '1':
      doorClose();
      break;
    default:
      break;
  }
}

void evaluateSettings(char key, String cmd) {
  switch (key) {
    case '0':
      break;
    case '1':
      preferences.begin("Settings", false);
      printData("masterUUID", preferences.getString("masterUuid", ""));

      if (preferences.getString("masterUuid", "0") == "0") {
        preferences.putString("masterUuid", String(cmd));
      }

      printData("masterUUID", preferences.getString("masterUuid", ""));
      break;
    case '2':
      preferences.begin("Settings", false);
      preferences.putString("ssid", String(cmd));
      break;
    case '3':
      preferences.begin("Settings", false);
      preferences.putString("password", String(cmd));
      break;
    case '4':
      preferences.begin("Settings", false);
      preferences.putString("bleMacAddress", String(cmd));
      break;
    default:
      break;
  }

  preferences.end();
}

String decrypt(String text) {
  const String cipherText = text;
  String cipherFinish;

  for (int i = 0; i < cipherText.length(); i++) {
    char letter = cipherText[i];
    const bool upper = isUpperCase(letter);

    if (upper) {
      letter = alphabet[alphabetUpper.indexOf(letter)];
    }

    int index = alphabet.indexOf(letter);
    if (index == -1) {
      cipherFinish += letter;
    } else {
      index = ((index - cipherOffset) + alphabet.length());
      char nextLetter = fullAlphabet[index];

      if (upper) {
        nextLetter = alphabetUpper[alphabet.indexOf(nextLetter)];
      }

      cipherFinish += nextLetter;
    }
  }

  return cipherFinish;
}

String encrypt(String text) {
  const String cipherText = text;
  String cipherFinish;

  for (int i = 0; i < cipherText.length(); i++) {
    char letter = cipherText[i];
    const bool upper = isUpperCase(letter);

    if (upper) {
      letter = alphabet[alphabetUpper.indexOf(letter)];
    }

    int index = alphabet.indexOf(letter);
    if (index == -1) {
      cipherFinish += letter;
    } else {
      index = ((index + cipherOffset) + alphabet.length());
      char nextLetter = fullAlphabet[index];

      if (upper) {
        nextLetter = alphabetUpper[alphabet.indexOf(nextLetter)];
      }

      cipherFinish += nextLetter;
    }
  }

  return cipherFinish;
}

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      printData("BLE server connected!");
      deviceConnected = true;
      BLEDevice::startAdvertising();
    };

    void onDisconnect(BLEServer* pServer) {
      printData("BLE server disconnected!");
      deviceConnected = false;
    }
};

class MyCallbacks : public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();

      if (value.length() > 0) {
        bleValue = "";

        for (int i = 0; i < value.length(); i++) {
          bleValue += value[i];
        }

        // bleValue = decrypt(bleValue);

        Serial.println(bleValue);

        switch (bleValue[0]) {
          case 'C':
            printData("COMMANDS", bleValue);
            evaluateCommands(bleValue[1], bleValue.substring(3));
            break;
          case 'L':
            printData("LOCK", bleValue);
            break;
          case 'B':
            printData("BATTERY", bleValue);
            break;
          case 'N':
            printData("NETWORK", bleValue);
            break;
          case 'R':
            printData("RESET", bleValue);
            break;
          case 'S':
            printData("SETTINGS", bleValue);
            evaluateSettings(bleValue[1], bleValue.substring(3));
            break;
          case 'E':
            printData("ERRORS", bleValue);
            break;
          default:
            printData("DEFAULT", bleValue);
            break;
        }

      }
    }
};

void setupBLE() {
  // Create the BLE Device
  BLEDevice::init("GATEWAY-NOQEY");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY |
                      BLECharacteristic::PROPERTY_INDICATE
                    );

  pCharacteristic->setCallbacks(new MyCallbacks());

  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
}

void startBleServer() {
  // Start the service
  pService->start();

  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  BLEDevice::startAdvertising();

}

void sendNotifyData(const char* value) {
  // notify changed value
  if (deviceConnected) {
    pCharacteristic->setValue(value);
    pCharacteristic->notify();

    delay(10); // bluetooth stack will go into congestion, if too many packets are sent, in 6 hours test i was able to go as low as 3ms
  }
}

void sendBatteryLevel() {
  const float pilDeger = pil();
  printData("Pil Değeri", pilDeger);

  String batteryStatus = "B3";

  if (2048 <= pilDeger < 3072) {
    batteryStatus = "B2";
  }

  if (1024 <= pilDeger < 2048) {
    batteryStatus = "B1";
  }

  if (pilDeger < 1024) {
    batteryStatus = "B0";
  }

  batteryStatus = encrypt(batteryStatus);

  sendNotifyData(batteryStatus.c_str());
}

void loopBLE() {
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;

    sendBatteryLevel();
  }

  float motorVoltaji = MotorVoltaj();
  // printData("Motor Voltajı", motorVoltaji);

  const float motorDurmaYuzdesi = MotorDurmaYuzdesi(pil());

  if (MotorKalkisVoltaji - motorVoltaji > MotorKalkisVoltaji * motorDurmaYuzdesi) {
    // printData("Motor durdu", motorVoltaji);
    motorStop();
  }
}
