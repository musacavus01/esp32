int MAX = 0;
int MIN = 9999;

void calibrateHallSensor() {
  preferences.begin("sensorData", false);
  
  int count = 0;
  int measurement = 0;
  
  while(count < 20) {
    measurement = hallRead();

    if (abs(measurement) < MIN) {
      MIN = abs(measurement);
      preferences.putInt("hallSensorMin", abs(measurement));
    }
    
    count++;
    delay(500);
  }

  preferences.end();
}

int getHallSensorCalibratedData() {
  preferences.begin("sensorData", false);
  int min = preferences.getInt("hallSensorMin");
  preferences.end();
  
  return min;
}

bool testIsDoorOpen() {
  int count = 0;
  
  preferences.begin("sensorData", false);
  int min = preferences.getInt("hallSensorMin");
  preferences.end();
  
  while (hallRead() < min && count != 3) {
    count++;
    delay(200);
  };

  return count == 3 ? true : false;
}
