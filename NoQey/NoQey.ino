#include <fauxmoESP.h>

#include <Arduino.h>
#include <Preferences.h>
#include <ArduinoJson.h>

Preferences preferences;
//Own Libraries - Sıra önemli
#include "helpers.h"
#include "voltage.h"
#include "lock.h"
#include "hw_buttons.h"
#include "hall_sensor.h"
#include "setup.h"
#include "ble_server.h"
#include "web_socket.h"
#include "notifications.h"


#define USE_SERIAL Serial
#define TEST_SERVER true
fauxmoESP fauxmo;

String restURL = "http://167.71.40.136:4001";

void setup()
{
  Serial.begin(115200);

  preferences.begin("Settings", false);

  printData("PREFERENCES", preferences.getString("bleMacAddress", "0"));

  preferences.end();

  if (TEST_SERVER) {
    restURL += ":3001/api/v1";
  } else {
    restURL += ":3000/api/v1";
  }

  // motor çıkışlarını tanımlama
  pinMode(PWMA, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(STBY, OUTPUT);
  // Motor bacaklarını default değerleri

  setupHardwareButtons();
  motorStop();

  setupWifi();
  setupBLE();
  setupWebSocket();
  // printData("Hall sensor calibration started! - 10sn");

  // calibrateHallSensor();
}

void loop() {
  // printData("WiFi connection", isWifiConnect);
  Serial.println("loop");
  if (isWifiConnect == true) {
    loopWebSocket();
    clientBleLoop();
    
    preferences.begin("Settings", false);
    if (preferences.getString("ssid", "0") == "0") {
      disconnectWifi();
    }
    preferences.end();
  } else {
    setupWifi();
  }

  // printData("CALIBRATED", getHallSensorCalibratedData());

  /*if (hallRead() < getHallSensorCalibratedData() && isWifiConnect) {
    if (testIsDoorOpen()) {
      printData("KAPI AÇILDI");
      sendDoorNotification("openned");
    } else {
      printData("KAPI KAPANDI");
      sendDoorNotification("closed");
    }
    } */

  checkHardwareButtons();

  if (isBleClient) {
    startBleServer();
    isBleClient = false;
  }

  loopBLE();
  delay(1000);
}
