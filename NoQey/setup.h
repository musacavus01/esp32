#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>

boolean isWifiConnect = false;
WiFiMulti wifiMulti;

void setupWifi() {
  preferences.begin("Settings", false);

  const String ssid = preferences.getString("ssid", "0");
  const String password = preferences.getString("password", "0");

  preferences.end();

  Serial.println(ssid);
  Serial.println(password);

  if (ssid != "0" && password != "0") {
    Serial.println("girdim ");
    wifiMulti.addAP(ssid.c_str(), password.c_str());

    if (wifiMulti.run() == WL_CONNECTED) {
      printData("Baglanti kurdum");
      isWifiConnect = true;
    } else {
      isWifiConnect = false;
    }
  }
}

void disconnectWifi() {
  printData("DISCONNECTED");
  if (wifiMulti.run() == WL_CONNECTED) { // and it is connected then disconnect from wifi
    WiFi.mode(WIFI_MODE_NULL);
    isWifiConnect = false;
  }
}
